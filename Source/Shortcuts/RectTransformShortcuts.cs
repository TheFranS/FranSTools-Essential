﻿namespace UnityEngine
{
	public static class RectTransformShortcuts
	{
		public static void SetSizeDeltaX(
			this RectTransform rT,
			float x)
		{
			var sizeDelta = rT.sizeDelta;
			sizeDelta.x = x;
			rT.sizeDelta = sizeDelta;
		}

		public static void SetSizeDeltaY(
			this RectTransform rT,
			float y)
		{
			var sizeDelta = rT.sizeDelta;
			sizeDelta.y = y;
			rT.sizeDelta = sizeDelta;
		}

		public static void AddSizeDeltaX(
			this RectTransform rT,
			float x)
		{
			var sizeDelta = rT.sizeDelta;
			sizeDelta.x += x;
			rT.sizeDelta = sizeDelta;
		}

		public static void AddSizeDeltaY(
			this RectTransform rT,
			float y)
		{
			var sizeDelta = rT.sizeDelta;
			sizeDelta.y += y;
			rT.sizeDelta = sizeDelta;
		}

		public static void SetAnchoredPositionX(
			this RectTransform rT,
			float x)
		{
			var anchoredPosition = rT.anchoredPosition;
			anchoredPosition.x = x;
			rT.anchoredPosition = anchoredPosition;
		}

		public static void SetAnchoredPositionY(
			this RectTransform rT,
			float y)
		{
			var anchoredPosition = rT.anchoredPosition;
			anchoredPosition.y = y;
			rT.anchoredPosition = anchoredPosition;
		}

		public static void AddAnchoredPositionX(
			this RectTransform rT,
			float x)
		{
			var anchoredPosition = rT.anchoredPosition;
			anchoredPosition.x += x;
			rT.anchoredPosition = anchoredPosition;
		}

		public static void AddAnchoredPositionY(
			this RectTransform rT,
			float y)
		{
			var anchoredPosition = rT.anchoredPosition;
			anchoredPosition.y += y;
			rT.anchoredPosition = anchoredPosition;
		}

		public static void SetAnchoredPosition3DX(
			this RectTransform rT,
			float x)
		{
			var anchoredPosition3D = rT.anchoredPosition3D;
			anchoredPosition3D.x = x;
			rT.anchoredPosition3D = anchoredPosition3D;
		}

		public static void SetAnchoredPosition3DY(
			this RectTransform rT,
			float y)
		{
			var anchoredPosition3D = rT.anchoredPosition3D;
			anchoredPosition3D.y = y;
			rT.anchoredPosition3D = anchoredPosition3D;
		}

		public static void SetAnchoredPosition3DZ(
			this RectTransform rT,
			float z)
		{
			var anchoredPosition3D = rT.anchoredPosition3D;
			anchoredPosition3D.z = z;
			rT.anchoredPosition3D = anchoredPosition3D;
		}

		public static void SetPivotX(
			this RectTransform rT,
			float x)
		{
			var pivot = rT.pivot;
			pivot.x = x;
			rT.pivot = pivot;
		}

		public static void SetPivotY(
			this RectTransform rT,
			float y)
		{
			var pivot = rT.pivot;
			pivot.y = y;
			rT.pivot = pivot;
		}

		public static void SetAnchorMinX(
			this RectTransform rT,
			float x)
		{
			var anchorMin = rT.anchorMin;
			anchorMin.x = x;
			rT.anchorMin = anchorMin;
		}

		public static void SetAnchorMinY(
			this RectTransform rT,
			float y)
		{
			var anchorMin = rT.anchorMin;
			anchorMin.y = y;
			rT.anchorMin = anchorMin;
		}

		public static void SetAnchorMaxX(
			this RectTransform rT,
			float x)
		{
			var anchorMax = rT.anchorMax;
			anchorMax.x = x;
			rT.anchorMax = anchorMax;
		}

		public static void SetAnchorMaxY(
			this RectTransform rT,
			float y)
		{
			var anchorMax = rT.anchorMax;
			anchorMax.y = y;
			rT.anchorMax = anchorMax;
		}
	}
}
