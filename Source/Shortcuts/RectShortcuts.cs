﻿namespace UnityEngine
{
	public static class RectShortcuts
	{
		public static Rect SetX(
			this Rect r,
			float x)
		{
			r.x = x;
			return r;
		}
		
		public static Rect AddX(
			this Rect r,
			float x)
		{
			r.x += x;
			return r;
		}
		
		public static Rect SetY(
			this Rect r,
			float y)
		{
			r.y = y;
			return r;
		}
		
		public static Rect AddY(
			this Rect r,
			float y)
		{
			r.y += y;
			return r;
		}

		public static Rect SetWidth(
			this Rect r,
			float w)
		{
			r.width = w;
			return r;
		}

		public static Rect AddWidth(
			this Rect r,
			float w)
		{
			r.width += w;
			return r;
		}

		public static Rect SetHeight(
			this Rect r,
			float h)
		{
			r.height = h;
			return r;
		}
		
		public static Rect AddHeight(
			this Rect r,
			float h)
		{
			r.height += h;
			return r;
		}
	}
}