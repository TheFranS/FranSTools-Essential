﻿namespace UnityEngine
{
	public static class TransformShortcuts
	{
		public static void SetPositionX(
			this Transform t,
			float x)
		{
			var position = t.position;
			position.x = x;
			t.position = position;
		}

		public static void SetPositionY(
			this Transform t,
			float y)
		{
			var position = t.position;
			position.y = y;
			t.position = position;
		}

		public static void SetPositionZ(
			this Transform t,
			float z)
		{
			var position = t.position;
			position.z = z;
			t.position = position;
		}

		public static void AddPositionX(
			this Transform t,
			float x)
		{
			var position = t.position;
			position.x += x;
			t.position = position;
		}

		public static void AddPositionY(
			this Transform t,
			float y)
		{
			var position = t.position;
			position.y += y;
			t.position = position;
		}

		public static void AddPositionZ(
			this Transform t,
			float z)
		{
			var position = t.position;
			position.z += z;
			t.position = position;
		}
		
		public static void SetLocalPositionX(
			this Transform t,
			float x)
		{
			var localPosition = t.localPosition;
			localPosition.x = x;
			t.localPosition = localPosition;
		}

		public static void SetLocalPositionY(
			this Transform t,
			float y)
		{
			var localPosition = t.localPosition;
			localPosition.y = y;
			t.localPosition = localPosition;
		}

		public static void SetLocalPositionZ(
			this Transform t,
			float z)
		{
			var localPosition = t.localPosition;
			localPosition.z = z;
			t.localPosition = localPosition;
		}

		public static void AddLocalPositionX(
			this Transform t,
			float x)
		{
			var localPosition = t.localPosition;
			localPosition.x += x;
			t.localPosition = localPosition;
		}

		public static void AddLocalPositionY(
			this Transform t,
			float y)
		{
			var localPosition = t.localPosition;
			localPosition.y += y;
			t.localPosition = localPosition;
		}

		public static void AddLocalPositionZ(
			this Transform t,
			float z)
		{
			var localPosition = t.localPosition;
			localPosition.z += z;
			t.localPosition = localPosition;
		}

		public static void SetLocalScaleX(
			this Transform t,
			float x)
		{
			var localScale = t.localScale;
			localScale.x = x;
			t.localScale = localScale;
		}

		public static void SetLocalScaleY(
			this Transform t,
			float y)
		{
			var localScale = t.localScale;
			localScale.y = y;
			t.localScale = localScale;
		}

		public static void SetLocalScaleZ(
			this Transform t,
			float z)
		{
			var localScale = t.localScale;
			localScale.z = z;
			t.localScale = localScale;
		}
	}
}