﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

public class Serializer
{
	#region ----------- Serializer -----------

	// public static BinaryFormatter binaryFormatter = new BinaryFormatter();
	// public static MemoryStream memoryStream = new MemoryStream();

	#endregion

	#region ----------- String -----------

	public static string EncodeObjectToString(object serializableObject)
	{
		if (serializableObject == null)
		{
			return null;
		}

		MemoryStream memoryStream = new MemoryStream();
		BinaryFormatter binaryFormatter = new BinaryFormatter();

		binaryFormatter.Serialize(memoryStream, serializableObject);
		return Convert.ToBase64String(memoryStream.ToArray());
	}

	public static T DecodeStringToObject<T>(string serializedData)
	{
		if (serializedData == null)
		{
			return default(T);
		}

		BinaryFormatter binaryFormatter = new BinaryFormatter();

		MemoryStream dataStream = new MemoryStream(Convert.FromBase64String(serializedData));
		return (T)binaryFormatter.Deserialize(dataStream);
	}

	#endregion

	#region ----------- ByteArray -----------

	public static byte[] EncodeObjectToByteArray(object serializableObject)
	{
		if (serializableObject == null)
		{
			return null;
		}

		MemoryStream memoryStream = new MemoryStream();
		BinaryFormatter binaryFormatter = new BinaryFormatter();

		binaryFormatter.Serialize(memoryStream, serializableObject);
		return memoryStream.ToArray();
	}

	public static T DecodeByteArrayToObject<T>(byte[] serializedData)
	{
		if (serializedData == null)
		{
			return default(T);
		}

		MemoryStream memoryStream = new MemoryStream();
		BinaryFormatter binaryFormatter = new BinaryFormatter();

		memoryStream.Write(serializedData, 0, serializedData.Length);
		memoryStream.Seek(0, SeekOrigin.Begin);
		return (T)binaryFormatter.Deserialize(memoryStream);
	}

	#endregion
}