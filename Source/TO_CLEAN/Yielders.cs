﻿using System.Collections.Generic;
using UnityEngine;

public static class Yielders
{
	static Dictionary<float, WaitForSeconds> timeInterval = new Dictionary<float, WaitForSeconds>();
	static WaitForEndOfFrame endOfFrame = new WaitForEndOfFrame();
	static WaitForFixedUpdate fixedUpdate = new WaitForFixedUpdate();

	public static WaitForEndOfFrame EndOfFrame => endOfFrame;

	public static WaitForFixedUpdate FixedUpdate => fixedUpdate;

	public static WaitForSeconds Get(float seconds)
	{
		if (!timeInterval.ContainsKey(seconds))
		{
			timeInterval.Add(seconds, new WaitForSeconds(seconds));
		}

		return timeInterval[seconds];
	}
}