﻿using UnityEngine;

public static class Vector3Ext
{
	public static Vector3 Average(
		Vector3 vector1,
		Vector3 vector2)
	{
		return Average(new Vector3[] {vector1, vector2});
	}

	public static Vector3 Average(Vector3[] vectors)
	{
		Vector3 center = Vector3.zero;
		for (int i = 0; i < vectors.Length; i++)
		{
			center += vectors[i];
		}

		return center / vectors.Length;
	}

	public static Vector3 ProjectPointOnLine(
		Vector3 point,
		Vector3 startLine,
		Vector3 endLine)
	{
		return Vector3.Project((point - startLine), (endLine - startLine)) + startLine;
	}

	public static Vector3 ProjectPointOnDirectionLine(
		Vector3 point,
		Vector3 startLine,
		Vector3 directionLine)
	{
		return Vector3.Project((point - startLine), directionLine) + startLine;
	}

	public static Vector3 InverseXAxis(Vector3 original)
	{
		return new Vector3(-original.x, original.y, original.z);
	}

	public static float Angle360(
		Vector3 vectorReference,
		Vector3 vectorToAngle,
		Vector3 normal)
	{
		float angle = AngleNegative(vectorReference, vectorToAngle, normal);
		if (angle < 0)
		{
			angle += 360;
		}

		return angle;
	}

	public static float AngleNegative(
		Vector3 vectorReference,
		Vector3 vectorToAngle,
		Vector3 normal)
	{
		float angle = Vector3.Angle(vectorReference, vectorToAngle);
		Vector3 cross = Vector3.Cross(vectorReference, vectorToAngle);
		return angle * Mathf.Sign(Vector3.Dot(cross, normal));
	}

	public static Vector3 DefaultValue
	{
		get { return Vector3.one * 696969; }
	}

	public static bool IfDefault(float axis)
	{
		if (axis == 696969)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	/// <summary>
	/// Projette une direction par rapport à une normale.
	/// Utile pour un character controller qui gère les pentes.
	/// </summary>
	public static Vector3 ProjectDirectionOnNormal(
		Vector3 direction,
		Vector3 normal)
	{
		Vector3 projectRight = Quaternion.Euler(Vector3.forward * 90) * direction;
		Vector3 projectLeft = Quaternion.Euler(Vector3.forward * -90) * direction;
		Vector3 projectUp = direction;
		Vector3 projectForward = Quaternion.Euler(Vector3.right * -90) * direction;
		Vector3 projectBack = Quaternion.Euler(Vector3.right * 90) * direction;

		return ((
			        Mathf.Abs(normal.x) * ((Mathf.Sign(normal.x) == 1) ? projectLeft : projectRight) +
			        Mathf.Abs(normal.y) * projectUp +
			        Mathf.Abs(normal.z) * ((Mathf.Sign(normal.z) == 1) ? projectBack : projectForward)
		        ) / (Mathf.Abs(normal.x) + Mathf.Abs(normal.y) + Mathf.Abs(normal.z))
			).normalized;
	}

	public static Vector3 RemoveYAxis(Vector3 vector)
	{
		vector.y = 0;
		return vector.normalized;
	}

	public static Vector3 Median(
		Vector3 vector1,
		Vector3 vector2)
	{
		return Vector3.Lerp(vector1, vector2, 0.5f).normalized;
	}

	public static Vector3 SetX(
		this Vector3 vector,
		float x)
	{
		return new Vector3(x, vector.y, vector.z);
	}

	public static Vector3 SetY(
		this Vector3 vector,
		float y)
	{
		return new Vector3(vector.x, y, vector.z);
	}

	public static Vector3 SetZ(
		this Vector3 vector,
		float z)
	{
		return new Vector3(vector.x, vector.y, z);
	}
}