﻿using System.Collections.Generic;
using UnityEngine;

public static class ListExt
{
	public static void Move<T>(
		this List<T> list,
		int itemIndex,
		int newItemIndex)
	{
		if (itemIndex == newItemIndex)
		{
			return;
		}

		if (newItemIndex > itemIndex)
		{
			list.Insert(newItemIndex + 1, list[itemIndex]);
			list.RemoveAt(itemIndex);
		}
		else
		{
			list.Insert(newItemIndex, list[itemIndex]);
			list.RemoveAt(itemIndex + 1);
		}
	}

	public static void Shuffle<T>(this IList<T> deck)
	{
		for (int i = 0; i < deck.Count; i++)
		{
			T temp = deck[i];
			int randomIndex = Random.Range(0, deck.Count);
			deck[i] = deck[randomIndex];
			deck[randomIndex] = temp;
		}
	}
}