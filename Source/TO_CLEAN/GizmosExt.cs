﻿using UnityEditor;
using UnityEngine;

public sealed class GizmosExt
{
	public static void DrawText(
		string text,
		Vector3 worldPosition)
	{
#if UNITY_EDITOR
		Handles.BeginGUI();
		Color restoreColor = GUI.color;
		GUI.color = Gizmos.color;

		var view = SceneView.currentDrawingSceneView;
		if (view == null)
		{
			GUI.color = restoreColor;
			Handles.EndGUI();
			return;
		}

		Vector3 screenPos = view.camera.WorldToScreenPoint(worldPosition);
		if (screenPos.y < 0 || screenPos.y > Screen.height || screenPos.x < 0 || screenPos.x > Screen.width ||
		    screenPos.z < 0)
		{
			GUI.color = restoreColor;
			Handles.EndGUI();
			return;
		}

		Vector2 size = GUI.skin.label.CalcSize(new GUIContent(text));
		GUI.Label(new Rect(screenPos.x - (size.x / 2), -screenPos.y + view.position.height + 4, size.x, size.y), text);
		GUI.color = restoreColor;
		Handles.EndGUI();
#endif // if UNITY_EDITOR
	}

	public static void DrawPlane(
		Vector3 position,
		Vector3 direction,
		Vector2 size)
	{
		Quaternion rotation = Quaternion.LookRotation(direction);
		Matrix4x4 trs = Matrix4x4.TRS(position, rotation, Vector3.one);
		Gizmos.matrix = trs;
		Gizmos.DrawCube(Vector3.zero, new Vector3(size.x, size.y, 0.0001f));
		Gizmos.matrix = Matrix4x4.identity;
	}

	public static void DrawWireRotateCube(
		Vector3 center,
		Vector3 size,
		Vector3 direction)
	{
		DrawWireRotateCube(center, size, Quaternion.LookRotation(direction, Vector3.up));
	}

	public static void DrawWireRotateCube(
		Vector3 center,
		Vector3 size,
		Vector3 direction,
		Vector3 upward)
	{
		DrawWireRotateCube(center, size, Quaternion.LookRotation(direction, upward));
	}

	public static void DrawWireRotateCube(
		Vector3 center,
		Vector3 size,
		Quaternion rotation)
	{
		Gizmos.matrix = Matrix4x4.TRS(center, rotation, Vector3.one);
		Gizmos.DrawWireCube(Vector3.zero, size);
		Gizmos.matrix = Matrix4x4.identity;
	}
}