﻿using UnityEngine;
using System;

public static class Vector2Ext
{
	public static Vector2 Average(
		Vector2 vector1,
		Vector2 vector2)
	{
		return Average(new Vector2[] {vector1, vector2});
	}

	public static Vector2 Average(Vector2[] vectors)
	{
		Vector2 center = Vector2.zero;
		for (int i = 0; i < vectors.Length; i++)
		{
			center += vectors[i];
		}

		return center / vectors.Length;
	}

	public static Vector2 AverageCoef(
		Vector2 vector1,
		float coef1,
		Vector2 vector2,
		float coef2)
	{
		if (coef1 == 0 && coef2 == 0)
		{
			return (vector1 + vector2) / 2f;
		}

		if (coef1 == 0)
		{
			return vector2;
		}

		if (coef2 == 0)
		{
			return vector1;
		}
		else
		{
			return (vector1 * coef1 + vector2 * coef2) / (coef1 + coef2);
		}
	}

	public static Vector2 SmoothDampAngle(
		Vector2 current,
		Vector2 target,
		ref Vector2 currentVelocity,
		float smoothTime)
	{
		return new Vector2(
			Mathf.SmoothDampAngle(current.x, target.x, ref currentVelocity.x, smoothTime),
			Mathf.SmoothDampAngle(current.y, target.y, ref currentVelocity.y, smoothTime)
		);
	}

	public static Vector2 SetX(
		this Vector2 vector,
		float x)
	{
		return new Vector2(x, vector.y);
	}

	public static Vector2 SetY(
		this Vector2 vector,
		float y)
	{
		return new Vector2(vector.x, y);
	}

	/// <summary>
	/// Gets the coordinates of the intersection point of two lines.
	/// </summary>
	/// <param name="a1">A point on the first line.</param>
	/// <param name="a2">Another point on the first line.</param>
	/// <param name="b1">A point on the second line.</param>
	/// <param name="b2">Another point on the second line.</param>
	/// <param name="found">Is set to false of there are no solution. true otherwise.</param>
	/// <returns>The intersection point coordinates. Returns Vector2.zero if there is no solution.</returns>
	public static Vector2 GetIntersectionPointCoordinates(
		Vector2 a1,
		Vector2 a2,
		Vector2 b1,
		Vector2 b2,
		out bool found)
	{
		// Reference: https://blog.dakwamine.fr/?p=1943
		float tmp = (b2.x - b1.x) * (a2.y - a1.y) - (b2.y - b1.y) * (a2.x - a1.x);

		if (tmp == 0)
		{
			// No solution!
			found = false;
			return Vector2.zero;
		}

		float mu = ((a1.x - b1.x) * (a2.y - a1.y) - (a1.y - b1.y) * (a2.x - a1.x)) / tmp;

		found = true;

		return new Vector2(
			b1.x + (b2.x - b1.x) * mu,
			b1.y + (b2.y - b1.y) * mu
		);
	}

	public static Vector3[] ToVector3Array(Vector2[] array)
	{
		return Array.ConvertAll(array, ToVector3);
	}

	private static Vector3 ToVector3(Vector2 vector2)
	{
		return new Vector3(vector2.x, vector2.y, 0);
	}

	/// <summary>
    /// Return true if from and to are in the clockwise direction
    /// </summary>
    public static bool Clockwise(Vector2 from, Vector2 to)
    {
        Vector2 a = from.normalized;
        Vector2 b = to.normalized;
        
        return Mathf.Sign(b.x * a.y - b.y * a.x) > 0f &&
               !Mathf.Approximately(Mathf.Abs(Vector2.Dot(a, b)), 1f);
    }
}