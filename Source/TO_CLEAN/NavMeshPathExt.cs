﻿using UnityEngine;
using UnityEngine.AI;

public static class NavMeshPathExt
{
	public static Vector3[] AddOffsetCorners(
		Vector3[] corners,
		float offset)
	{
		Vector3[] newCorners = new Vector3[corners.Length];

		for (int i = 0; i < corners.Length; i++)
		{
			if (i == 0 || i == corners.Length - 1)
			{
				newCorners[i] = corners[i];
			}
			else
			{
				Vector3 vector1 = corners[i - 1] - corners[i];
				Vector3 vector2 = corners[i + 1] - corners[i];
				Vector3 vectorMedian = Vector3Ext.Median(vector1, vector2);
				newCorners[i] = corners[i] - vectorMedian * offset;
			}
		}

		return newCorners;
	}

	/// <summary>
	/// Retourne la longueur du chemin
	/// </summary>
	public static float GetPathLenght(this NavMeshPath passPath)
	{
		float newPathLenght = 0;

		// On vérifie si le chemin a plus d'un seul point
		if (passPath.corners.Length > 1)
		{
			for (int i = 1; i < passPath.corners.Length; i++)
			{
				newPathLenght += Vector3.Distance(passPath.corners[i], passPath.corners[i - 1]);
			}
		}

		return newPathLenght;
	}
}