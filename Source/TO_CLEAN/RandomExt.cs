﻿using Random = UnityEngine.Random;
using System;
using System.Collections.Generic;

public static class RandomExt
{
	public static int RangeWeight(List<int> weightArray)
	{
		if (weightArray == null || weightArray.Count == 0)
		{
			throw new Exception("Weight Array is invalid!");
		}

		int weightSum = 0;

		for (int i = 0; i < weightArray.Count; i++)
		{
			if (weightArray[i] <= 0)
			{
				throw new ArithmeticException();
			}

			weightSum += weightArray[i];
		}

		int linearRandom = Random.Range(1, weightSum + 1);
		int accumulatedWeight = weightArray[0];

		for (int i = 0; i < weightArray.Count - 1; i++)
		{
			if (linearRandom <= accumulatedWeight)
			{
				return i;
			}

			accumulatedWeight += weightArray[i + 1];
		}

		return weightArray.Count - 1;
	}
}