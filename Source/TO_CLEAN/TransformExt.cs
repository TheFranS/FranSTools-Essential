﻿using UnityEngine;

public static class TransformExt
{
	public static Transform[] GetChildrens(this Transform transform)
	{
		Transform[] childrens = new Transform[transform.childCount];
		for (int i = 0; i < childrens.Length; i++)
		{
			childrens[i] = transform.GetChild(i);
		}

		return childrens;
	}

	public static Transform[] GetChildrensRecursive(this Transform transform)
	{
		return transform.GetComponentsInChildren<Transform>();
	}

	public static void SetLayerWithChildrensRecursive(
		this Transform transform,
		int layer)
	{
		Transform[] childrens = GetChildrensRecursive(transform);
		for (int i = 0; i < childrens.Length; i++)
		{
			childrens[i].gameObject.layer = layer;
		}

		transform.gameObject.layer = layer;
	}
}