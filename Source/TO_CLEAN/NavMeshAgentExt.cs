﻿using UnityEngine;
using UnityEngine.AI;

public static class NavMeshAgentExt
{
	/// <summary>
	/// Retourne le chemin vers la destination passé en argument
	/// </summary>
	public static NavMeshPath GetPath(
		this NavMeshAgent agent,
		Vector3 passDestination)
	{
		NavMeshPath newPath = new NavMeshPath();
		agent.CalculatePath(passDestination, newPath);
		return newPath;
	}

	/// <summary>
	/// Calcule la distance jusqu'à la destination passé en argument en utilisant un chemin
	/// </summary>
	public static float GetDistanceWithPath(
		this NavMeshAgent agent,
		Vector3 passDestination)
	{
		return agent.GetPath(passDestination).GetPathLenght();
	}
}