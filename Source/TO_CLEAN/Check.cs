﻿using System;
using UnityEditor;

public static class Check
{
	public static void IsTrue(
		bool condition,
		string message = "")
	{
		if (condition)
		{
			return;
		}

		if (string.IsNullOrWhiteSpace(message))
		{
			message = "IsTrue failed!";
		}
		else
		{
			message = "IsTrue failed with message: " + message;
		}

		ThrowError(message);
	}

	public static void Crash(string message)
	{
		if (string.IsNullOrWhiteSpace(message))
		{
			message = "Crash!";
		}
		else
		{
			message = "Crash with message: " + message;
		}

		ThrowError(message);
	}

	private static void ThrowError(string message)
	{
#if UNITY_EDITOR

		if (EditorApplication.isPlaying || EditorApplication.isPlayingOrWillChangePlaymode)
		{
			EditorApplication.isPlaying = false;
		}

		string[] stackTrace = Environment.StackTrace.Split('\n');
		string finalStackTrace = "";

		if (stackTrace.Length > 3)
		{
			for (int i = 3; i < stackTrace.Length; i++)
			{
				if (finalStackTrace != "")
				{
					finalStackTrace += "\n";
				}

				finalStackTrace += stackTrace[i];
			}
		}

		if (finalStackTrace.Length > 1000)
        {
            finalStackTrace = finalStackTrace.Substring(0, 1000);
        }

		EditorUtility.DisplayDialog("Fatal error!", message + "\n\n" + finalStackTrace, "Ok! I will fix that!");

#else // if UNITY_EDITOR
		UnityEngine.Application.Quit();

#endif // if UNITY_EDITOR

		throw new Exception(message);
	}
}