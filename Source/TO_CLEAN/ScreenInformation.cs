﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace FranSTools.Essential
{
	[RequireComponent(typeof(CanvasScaler))]
	public class ScreenInformation : MonoBehaviourSingleton<ScreenInformation>
	{
		private const float width = 1080f;
		private const float height = 1920f;

		private CanvasScaler _cs;

		private readonly Vector2 _referenceResolutionPortrait = new Vector2(width, height);
		private readonly Vector2 _referenceResolutionLandscape = new Vector2(height, width);

		private const float MatchPortrait = 0f;
		private const float MatchLandscape = 1f;

		private ScreenOrientation _previousOrientation;
		public Action<ScreenOrientation> OnOrientationChange;

#if UNITY_EDITOR
		private float _previousRatio;
		public Action<float> OnRatioChange;
#endif

		public Action OnChange;

		public static ScreenOrientation Orientation
		{
			get
			{
#if UNITY_EDITOR
				return Screen.width > Screen.height ? ScreenOrientation.Landscape : ScreenOrientation.Portrait;
#else
				switch (Screen.orientation)
				{
				case ScreenOrientation.Portrait:
				case ScreenOrientation.PortraitUpsideDown:
					return ScreenOrientation.Portrait;

				case ScreenOrientation.LandscapeLeft:
				case ScreenOrientation.LandscapeRight:
					return ScreenOrientation.Landscape;

				default:
					return Screen.orientation;
				}
#endif // if UNITY_EDITOR
			}
		}

		public static float Ratio => (float)Screen.width / Screen.height;

		public float CanvasRealWidth
		{
			get
			{
				float matchWidth = _cs.referenceResolution.x * (1 - _cs.matchWidthOrHeight);
				float matchHeight = (float)Screen.width / Screen.height * _cs.referenceResolution.y *
				                    _cs.matchWidthOrHeight;

				return matchWidth + matchHeight;
			}
		}

		public float CanvasRealHeight
		{
			get
			{
				float matchWidth = _cs.referenceResolution.y * _cs.matchWidthOrHeight;
				float matchHeight = (float)Screen.height / Screen.width * _cs.referenceResolution.x *
				                    (1 - _cs.matchWidthOrHeight);

				return matchWidth + matchHeight;
			}
		}

		protected override void SingletonAwake()
		{
			_cs = GetComponent<CanvasScaler>();
			Check.IsTrue(_cs != null);

			_previousOrientation = Orientation;
			ChangeCanvasScalerParameters();

#if UNITY_EDITOR
			_previousRatio = Ratio;
#endif
		}

		private void Update()
		{
			if (_previousOrientation != Orientation)
			{
				_previousOrientation = Orientation;
				ChangeCanvasScalerParameters();

				OnOrientationChange?.Invoke(Orientation);
				OnChange?.Invoke();
			}

#if UNITY_EDITOR
			if (!Mathf.Approximately(_previousRatio, Ratio))
			{
				_previousRatio = Ratio;

				OnRatioChange?.Invoke(Ratio);
				OnChange?.Invoke();
			}
#endif
		}

		private void ChangeCanvasScalerParameters()
		{
			if (Orientation == ScreenOrientation.Landscape)
			{
				_cs.referenceResolution = _referenceResolutionLandscape;
				_cs.matchWidthOrHeight = MatchLandscape;
			}
			else
			{
				_cs.referenceResolution = _referenceResolutionPortrait;
				_cs.matchWidthOrHeight = MatchPortrait;
			}
		}

		public Vector2 WorldToScreenPoint(Vector3 position)
		{
			Vector2 screenPoint = Camera.main.WorldToScreenPoint(position);

			screenPoint.x *= CanvasRealWidth / Screen.width;
			screenPoint.y *= CanvasRealHeight / Screen.height;

			return screenPoint;
		}
	}
} // namespace FranSTools.Essential