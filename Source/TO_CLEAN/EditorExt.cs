﻿public static class EditorExt
{
	public static bool IsPlaying
	{
		get
		{
#if UNITY_EDITOR
			if (UnityEditor.EditorApplication.isPlaying ||
			    UnityEditor.EditorApplication.isPlayingOrWillChangePlaymode)
			{
#endif
				return true;
#if UNITY_EDITOR
			}
			else
			{
				return false;
			}
#endif // if UNITY_EDITOR
		}
	}
}