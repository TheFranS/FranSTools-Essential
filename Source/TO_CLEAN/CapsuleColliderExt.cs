﻿using UnityEngine;

public static class CapsuleColliderExt
{
	/// <summary>
	/// Retourne la position de la sphère basse de la capsule
	/// </summary>
	public static Vector3 GetBottomPosition(this CapsuleCollider capsuleCollider)
	{
		return capsuleCollider.GetBottomPosition(capsuleCollider.transform);
	}

	/// <summary>
	/// Retourne la position de la sphère basse de la capsule
	/// </summary>
	public static Vector3 GetBottomPosition(
		this CapsuleCollider capsuleCollider,
		Transform transform)
	{
		if (transform == null)
		{
			Debug.LogError("Aucun transform n'est renseigné !");
			return Vector3.zero;
		}

		if (capsuleCollider.direction != 1)
		{
			Debug.LogError("La direction de la capsule n'est pas Y !");
			return Vector3.zero;
		}

		Vector3 bottomPosition = transform.position + transform.rotation * capsuleCollider.center;
		if (capsuleCollider.height >= capsuleCollider.radius * 2)
		{
			bottomPosition += transform.up * (-capsuleCollider.height / 2 + capsuleCollider.radius);
		}

		return bottomPosition;
	}

	/// <summary>
	/// Retourne la position du transform par rapport à la position de la sphère basse de la capsule
	/// </summary>
	public static Vector3 GetTransformPositionFromBottomPosition(
		this CapsuleCollider capsuleCollider,
		Vector3 bottomPosition)
	{
		return capsuleCollider.GetTransformPositionFromBottomPosition(capsuleCollider.transform, bottomPosition);
	}

	/// <summary>
	/// Retourne la position du transform par rapport à la position de la sphère basse de la capsule
	/// </summary>
	public static Vector3 GetTransformPositionFromBottomPosition(
		this CapsuleCollider capsuleCollider,
		Transform transform,
		Vector3 bottomPosition)
	{
		if (transform == null)
		{
			Debug.LogError("Aucun transform n'est renseigné !");
			return Vector3.zero;
		}

		if (capsuleCollider.direction != 1)
		{
			Debug.LogError("La direction de la capsule n'est pas Y !");
			return Vector3.zero;
		}

		Vector3 transformPosition = bottomPosition - transform.rotation * capsuleCollider.center;
		if (capsuleCollider.height >= capsuleCollider.radius * 2)
		{
			transformPosition -= transform.up * (-capsuleCollider.height / 2 + capsuleCollider.radius);
		}

		return transformPosition;
	}
}