﻿namespace FranS.PhysicsExt
{
	using System.Collections.Generic;
	using UnityEngine;

	public class CompoundTrigger : MonoBehaviour
	{
		List<GameObject> gameObjectInTrigger = new List<GameObject>();

		void OnTriggerEnter(Collider other)
		{
			GameObject enter = other.gameObject;
			if (!gameObjectInTrigger.Contains(enter))
			{
				OnCompoundTriggerEnter(enter);
			}

			gameObjectInTrigger.Add(enter);
		}

		void OnTriggerExit(Collider other)
		{
			GameObject exit = other.gameObject;
			gameObjectInTrigger.Remove(exit);
			if (!gameObjectInTrigger.Contains(exit))
			{
				OnCompoundTriggerExit(exit);
			}
		}

		public virtual void OnCompoundTriggerEnter(GameObject other)
		{
		}

		public virtual void OnCompoundTriggerExit(GameObject other)
		{
		}
	}
} // namespace FranS.PhysicsExt