﻿using UnityEngine;

public class PhysicsExt
{
	/// <summary>
	/// Projette une sphère et retourne la position du centre de la sphère à l'endroit du contact.
	/// Cette sphère ne pénètre aucun colliders.
	/// </summary>
	/// <returns>The cast with returned center.</returns>
	/// <param name="origin">Origin.</param>
	/// <param name="radius">Radius.</param>
	/// <param name="direction">Direction.</param>
	/// <param name="maxDistance">Max distance.</param>
	/// <param name="offset">Offset.</param>
	public static SphereCastReturnedCenter SphereCastWithReturnedCenter(
		Vector3 origin,
		float radius,
		Vector3 direction,
		float maxDistance,
		float offset,
		LayerMask layerMask)
	{
		direction.Normalize();
		if (offset < 0)
		{
			offset = 0;
		}

		if (maxDistance <= 0 || radius <= 0)
		{
			return new SphereCastReturnedCenter(false, origin, 0f);
		}

		Vector3 result = origin + direction * maxDistance;
		bool hasTouched = false;
		RaycastHit hit;

		if (Physics.SphereCast(origin, radius, direction, out hit, maxDistance + offset, layerMask))
		{
			result = hit.point + hit.normal * radius;
			hasTouched = true;

			if (Vector3.Distance(result, origin) > offset)
			{
				result += -direction * offset;
			}
			else
			{
				result = origin;
			}
		}

		return new SphereCastReturnedCenter(hasTouched, result, Vector3.Distance(origin, result));
	}

	public static SphereCastReturnedCenter SphereCastWithReturnedCenter(
		Vector3 startPoint,
		float radius,
		Vector3 endPoint,
		float offset,
		LayerMask layerMask)
	{
		Vector3 direction = endPoint - startPoint;
		return SphereCastWithReturnedCenter(startPoint, radius, direction, direction.magnitude, offset, layerMask);
	}
}

public struct SphereCastReturnedCenter
{
	bool hasTouched;
	Vector3 point;
	float distance;

	public SphereCastReturnedCenter(
		bool hasTouched,
		Vector3 point,
		float distance)
	{
		this.hasTouched = hasTouched;
		this.point = point;
		this.distance = distance;
	}

	public bool HasTouched
	{
		get { return this.hasTouched; }
	}

	public Vector3 Point
	{
		get { return this.point; }
	}

	public float Distance
	{
		get { return this.distance; }
	}
}