﻿using UnityEngine;

public static class CameraExt
{
	public static Vector3 WorldToScreenPointVirtual(
		this Camera camera,
		Vector3 worldPosition)
	{
		Vector3 realScreenPoint = Camera.main.WorldToScreenPoint(worldPosition);

		float screenHeightReal = (float)Screen.height;
		float screenWidthReal = (float)Screen.width;
		float screenHeightVirtual = 1080f;
		float screenWidthVirtual = (screenWidthReal / screenHeightReal) * screenHeightVirtual;

		return new Vector3(
			realScreenPoint.x * (screenWidthVirtual / screenWidthReal),
			realScreenPoint.y * (screenHeightVirtual / screenHeightReal),
			realScreenPoint.z
		);
	}

	public static Vector2 ScreenRealToScreenPointVirtual(
		this Camera camera,
		Vector3 screenPosition)
	{
		float screenHeightReal = (float)Screen.height;
		float screenWidthReal = (float)Screen.width;
		float screenHeightVirtual = 1080f;
		float screenWidthVirtual = (screenWidthReal / screenHeightReal) * screenHeightVirtual;

		return new Vector2(
			screenPosition.x * (screenWidthVirtual / screenWidthReal),
			screenPosition.y * (screenHeightVirtual / screenHeightReal)
		);
	}
}