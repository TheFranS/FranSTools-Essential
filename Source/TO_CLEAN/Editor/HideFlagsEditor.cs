﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

public class HideFlagsEditor : EditorWindow
{
	[MenuItem("Tools/HideFlagsEditor")]
	static void Init()
	{
		// Use only one window
		GetWindow<HideFlagsEditor>();

		//// allows multiple windows
		// CreateInstance<HideFlagsEditor>().Show();
	}

	Object[] objects;
	Object[] hiddenObjects;
	Vector2 pos1;
	Vector2 pos2;
	Rect r1;
	Rect r2;

	bool displayGameObjects = true;
	bool displayComponents = false;
	bool displayAssets = false;

	void OnEnable()
	{
		Selection.selectionChanged += OnSelectionChanged;
		OnSelectionChanged();
	}

	void OnDisable()
	{
		Selection.selectionChanged -= OnSelectionChanged;
	}

	void OnSelectionChanged()
	{
		objects = Selection.objects;
		IEnumerable<Object> c = Resources.FindObjectsOfTypeAll<Object>();
		c = c.Where(a => (a.hideFlags & (HideFlags.HideInHierarchy | HideFlags.HideInInspector)) > 0);
		if (!displayGameObjects)
		{
			c = c.Where(a => !(a is GameObject));
		}

		if (!displayComponents)
		{
			c = c.Where(a => !(a is Component));
		}

		if (!displayAssets)
		{
			c = c.Where(a => (a is GameObject) || (a is Component));
		}

		hiddenObjects = c.ToArray();
		Repaint();
		EditorApplication.RepaintHierarchyWindow();
	}

	void FilterToggle(
		ref bool aValue,
		string aTitle)
	{
		var newVal = GUILayout.Toggle(aValue, aTitle, "button");
		if (newVal != aValue)
		{
			aValue = newVal;
			OnSelectionChanged();
		}
	}

	HideFlags HFField(
		HideFlags flags,
		HideFlags item)
	{
		bool state = (flags & item) == item;
		if (GUILayout.Toggle(state, "" + item, "button") ^ state)
		{
			state = !state;
			if (state)
			{
				flags |= item;
			}
			else
			{
				flags &= ~item;
			}
		}

		return flags;
	}

	HideFlags HFFields(HideFlags hf)
	{
		GUILayout.BeginVertical();
		GUILayout.BeginHorizontal();
		hf = HFField(hf, HideFlags.DontSaveInBuild);
		hf = HFField(hf, HideFlags.DontSaveInEditor);
		hf = HFField(hf, HideFlags.DontUnloadUnusedAsset);
		GUILayout.EndHorizontal();

		GUILayout.BeginHorizontal();
		hf = HFField(hf, HideFlags.HideInHierarchy);
		hf = HFField(hf, HideFlags.HideInInspector);
		hf = HFField(hf, HideFlags.NotEditable);

		GUILayout.EndHorizontal();
		GUILayout.BeginHorizontal();
		hf = HFField(hf, HideFlags.DontSave);
		hf = HFField(hf, HideFlags.HideAndDontSave);
		GUILayout.EndHorizontal();
		GUILayout.EndVertical();
		return hf;
	}

	void OnGUI()
	{
		Rect rect = GUILayoutUtility.GetRect(10, 20000, 10, 20000);
		BeginWindows();
		r1 = GUI.Window(0, r1, OnUpperWindow, "", "label");
		r2 = GUI.Window(1, r2, OnLowerWindow, "", "label");
		EndWindows();
		if (Event.current.type != EventType.Layout && rect.width > 1)
		{
			if (r2.width <= 0)
			{
				r2.yMin = rect.height * 0.5f;
			}

			r2.yMin = Mathf.Clamp(r2.yMin, rect.yMin, rect.yMax - 20);
			r1.x = r2.x = 0;
			r1.width = r2.width = rect.width;
			r1.yMax = r2.yMin;
			r2.yMax = rect.yMax;
		}
	}

	void OnUpperWindow(int id)
	{
		if (objects != null && objects.Length > 0)
		{
			pos1 = GUILayout.BeginScrollView(pos1);
			foreach (var o in objects)
			{
				if (o == null)
				{
					continue;
				}

				var hf = o.hideFlags;
				GUILayout.BeginHorizontal("box");
				GUILayout.Label(o.name + "\n  type: " + o.GetType().Name);
				GUILayout.FlexibleSpace();
				if (GUILayout.Button("Deselect"))
				{
					var list = new List<Object>(Selection.objects);
					if (list.Contains(o))
					{
						list.Remove(o);
					}
					else
					{
						list.Add(o);
					}

					Selection.objects = list.ToArray();
				}

				var newHF = HFFields(hf);
				if (hf != newHF)
				{
					o.hideFlags = newHF;
				}

				GUILayout.EndHorizontal();
			}

			GUILayout.EndScrollView();
		}
		else
		{
			GUILayout.Box(" -- Select something -- ");
		}
	}

	void OnLowerWindow(int id)
	{
		GUILayout.Box("", GUILayout.Height(15), GUILayout.ExpandWidth(true));
		GUI.DragWindow(new Rect(0, 0, 10000, 15));
		EditorGUIUtility.AddCursorRect(new Rect(0, 0, 10000, 15), MouseCursor.ResizeVertical);
		if (hiddenObjects != null)
		{
			GUILayout.BeginHorizontal();
			GUILayout.Label("HiddenObjects");
			FilterToggle(ref displayGameObjects, "GameObjects");
			FilterToggle(ref displayComponents, "Components");
			FilterToggle(ref displayAssets, "Other Assets");
			GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal();

			pos2 = GUILayout.BeginScrollView(pos2);
			foreach (var o in hiddenObjects)
			{
				if (o == null)
				{
					continue;
				}

				GUILayout.BeginHorizontal("box");
				GUILayout.Label(o.name + " (" + o.GetType().Name + ")");
				GUILayout.FlexibleSpace();
				if (GUILayout.Button("select"))
				{
					var list = new List<Object>(Selection.objects);
					if (list.Contains(o))
					{
						list.Remove(o);
					}
					else
					{
						list.Add(o);
					}

					Selection.objects = list.ToArray();
				}

				GUILayout.EndHorizontal();
			}

			GUILayout.EndScrollView();
		}
	}
}