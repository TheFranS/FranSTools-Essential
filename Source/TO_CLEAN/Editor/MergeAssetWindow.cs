using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

public class MergeAssetWindow : EditorWindow
{
    private ReorderableList _findObjectList;
    private readonly List<Object> findObject = new List<Object>();
    private Object replaceObject;

    [MenuItem("Tools/AssetBundle/Merge Asset Window", false, 0)]
    private static void Open()
    {
        var window = (MergeAssetWindow) GetWindow(typeof(MergeAssetWindow));
        window.titleContent = new GUIContent("MergeAssetWindow");
        window.Show();
    }

    private void OnEnable()
    {
        _findObjectList = new ReorderableList(findObject, typeof(Object), true, true, true, true)
        {
            drawHeaderCallback = rect => { EditorGUI.LabelField(rect, "Find Objects", EditorStyles.boldLabel); }
        };

        _findObjectList.drawElementCallback = (rect, index, isActive, isFocused) =>
        {
            findObject[index] =
                EditorGUI.ObjectField(rect, index.ToString(), findObject[index], typeof(Object), false);
        };
    }

    private void OnGUI()
    {
        _findObjectList.DoLayoutList();
        replaceObject = EditorGUILayout.ObjectField("Replace", replaceObject, typeof(Object), false);

        if (findObject == null || replaceObject == null) GUI.enabled = false;

        if (GUILayout.Button("Find asset and replace"))
        {
            var findObjectPath = findObject.Select(AssetDatabase.GetAssetPath).ToArray();
            var replaceObjectPath = AssetDatabase.GetAssetPath(replaceObject);

            var findObjectGUID = findObjectPath.Select(AssetDatabase.AssetPathToGUID).ToArray();
            var replaceObjectGUID = AssetDatabase.AssetPathToGUID(replaceObjectPath);

            var enumerateFiles = Directory.EnumerateFiles("Assets/", "*.*", SearchOption.AllDirectories);

            var count = enumerateFiles.Count();
            var i = 0;

            for (var j = 0; j < findObjectPath.Length; j++) AssetDatabase.DeleteAsset(findObjectPath[j]);

            foreach (var filePath in enumerateFiles)
            {
                var progress = (float) i / (count + 1);
                EditorUtility.DisplayProgressBar("Find asset and replace", $"Handle {filePath}", progress);
                i++;

                var text = File.ReadAllText(filePath);
                var modify = false;

                for (var j = 0; j < findObjectGUID.Length; j++)
                    if (text.Contains(findObjectGUID[j]))
                    {
                        text = text.Replace(findObjectGUID[j], replaceObjectGUID);
                        modify = true;
                    }

                if (modify) File.WriteAllText(filePath, text);
            }

            EditorUtility.ClearProgressBar();
        }

        GUI.enabled = true;
    }
}