﻿using UnityEditor;
using UnityEngine;

public static class AssetDatabaseExt
{
	//[MenuItem("Assets/Force Reserialize", false, 0)]
	private static void ForceReserialize()
	{
	    Object activeObject = Selection.activeObject;

	    string relativeAssetPath = AssetDatabase.GetAssetPath(activeObject);

	    EditorUtility.DisplayProgressBar("Force Reserialize Assets", "Please wait...", 0f);

	    if (relativeAssetPath != "")
        {
            AssetDatabase.ForceReserializeAssets(new[]{relativeAssetPath});
        }
        else
        {
            AssetDatabase.ForceReserializeAssets();
        }

	    EditorUtility.DisplayProgressBar("Force Reserialize Assets", "Saves assets", 1f);
	    AssetDatabase.SaveAssets();
	    EditorUtility.ClearProgressBar();
	}

	//[MenuItem("Assets/GetImplicitAssetBundleName", false, 0)]
    private static void GetImplicitAssetBundleName()
    {
        Object activeObject = Selection.activeObject;
 
        string relativeAssetPath = AssetDatabase.GetAssetPath(activeObject);
        string implicitBundleName = AssetDatabase.GetImplicitAssetBundleName(relativeAssetPath);
		
        EditorUtility.DisplayDialog("Info", "Implicit bundle name is: "+implicitBundleName, "ok");
    }

	public static string AbsoluteToRelativePath(string absoluteFilePath)
	{
		return "Assets" + absoluteFilePath.Substring(Application.dataPath.Length);
	}
}