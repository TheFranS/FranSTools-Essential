﻿using System.Collections.Generic;
using System.Linq;
using UnityEditor;

public static class PlayerSettingsExt
{
	public static void AddScriptingDefineSymbols(string defineSymbol)
	{
		List<string> defineSymbols = GetListScriptingDefineSymbolsForGroup();

		int defineSymbolIndex = defineSymbols.FindIndex(e => e == defineSymbol);
		if (defineSymbolIndex == -1)
		{
			defineSymbols.Add(defineSymbol);
		}

		ApplyScriptingDefineSymbolsForGroup(defineSymbols);
	}

	public static void RemoveScriptingDefineSymbols(string defineSymbol)
	{
		List<string> defineSymbols = GetListScriptingDefineSymbolsForGroup();
		defineSymbols.RemoveAll(e => e == defineSymbol);
		ApplyScriptingDefineSymbolsForGroup(defineSymbols);
	}

	public static bool ScriptingDefineSymbolsExists(string defineSymbol)
	{
		string defineSymbolsString =
			PlayerSettings.GetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup);
		return defineSymbolsString.Split(';').Count(e => e == defineSymbol) > 0;
	}

	private static List<string> GetListScriptingDefineSymbolsForGroup()
	{
		string defineSymbolsString =
			PlayerSettings.GetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup);
		return defineSymbolsString.Split(';').ToList();
	}

	private static void ApplyScriptingDefineSymbolsForGroup(List<string> defineSymbols)
	{
		PlayerSettings.SetScriptingDefineSymbolsForGroup(
			EditorUserBuildSettings.selectedBuildTargetGroup,
			string.Join(";", defineSymbols));
	}
}