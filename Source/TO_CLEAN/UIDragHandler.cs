﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

public sealed class UIDragHandler : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    public Action<PointerEventData> OnBeginTouch;
    public Action<PointerEventData> OnTouch;
    public Action<PointerEventData> OnEndTouch;

    private bool isDragging = false;
    public bool IsDragging => isDragging;

    public void OnBeginDrag(PointerEventData eventData)
    {
        isDragging = true;
        OnBeginTouch?.Invoke(eventData);
    }

    public void OnDrag(PointerEventData eventData)
    {
        OnTouch?.Invoke(eventData);
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        isDragging = false;
        OnEndTouch?.Invoke(eventData);
    }
} // class DragHandler