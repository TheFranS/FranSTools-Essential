﻿using Random = UnityEngine.Random;
using System;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;

#endif

public static class MathExt
{
	/// <summary>
	/// Increments and loops the value between start (included) and end (excluded).
	/// Doesn't loop if start and end aren't correct.
	/// </summary>
	public static int Loop(
		this int value,
		int start,
		int end,
		int step = 0)
	{
		if (start >= end)
		{
			return value + step;
		}

		int newValue = value + step;
		int length = end - start;
		int modulo = (newValue - start) % length;

		while (modulo < 0)
		{
			modulo += length;
		}

		return start + modulo;
	}

	public static bool IsWithin(
		this int value,
		RangeInt range)
	{
		return value >= range.Min && value <= range.Max;
	}

	public static bool IsWithin(
		this float value,
		RangeFloat range)
	{
		return value >= range.Min && value <= range.Max;
	}

	public static bool SegmentsIntersect1D(
		RangeFloat x,
		RangeFloat y)
	{
		return x.Max >= y.Min && y.Max >= x.Min;
	}

	public static int Distance(
		int a,
		int b)
	{
		return Mathf.Abs(b - a);
	}

	public static float Distance(
		float a,
		float b)
	{
		return Mathf.Abs(b - a);
	}

	public static float Lerp3(
		float a,
		float b,
		float c,
		float aThreshold,
		float bThreshold,
		float cThreshold,
		float t)
	{
		if (t < bThreshold)
		{
			return Mathf.Lerp(a, b, Mathf.InverseLerp(aThreshold, bThreshold, t));
		}
		else
		{
			return Mathf.Lerp(b, c, Mathf.InverseLerp(bThreshold, cThreshold, t));
		}
	}

	public static float Lerp4(
		float a,
		float b,
		float c,
		float d,
		float aThreshold,
		float bThreshold,
		float cThreshold,
		float dThreshold,
		float t)
	{
		t = Mathf.Clamp(t, aThreshold, dThreshold);

		if (t < bThreshold)
		{
			return Mathf.Lerp(a, b, Mathf.InverseLerp(aThreshold, bThreshold, t));
		}

		if (t > bThreshold && t < cThreshold)
		{
			return Mathf.Lerp(b, c, Mathf.InverseLerp(bThreshold, cThreshold, t));
		}
		else
		{
			return Mathf.Lerp(c, d, Mathf.InverseLerp(cThreshold, dThreshold, t));
		}
	}

	public static float InverseLerp4(
		float a,
		float b,
		float c,
		float d,
		float t)
	{
		if (t < b)
		{
			return Mathf.InverseLerp(a, b, t);
		}
		else if (t > c)
		{
			return Mathf.InverseLerp(d, c, t);
		}
		else
		{
			return 1f;
		}
	}
}

[Serializable]
public struct RangeInt
{
	[SerializeField] private int _a;

	[SerializeField] private int _b;

	public RangeInt(
		int a,
		int b)
	{
		_a = a;
		_b = b;
	}

	public int Min => _a < _b ? _a : _b;
	public int Max => _b > _a ? _b : _a;

	public int RandomValue => Random.Range(Min, Max + 1);
	// public float RandomHalfValue => Random.Range(_min * 2, _max * 2 + 1) / 2f;

#if UNITY_EDITOR
	public static RangeInt EditorGetValue(SerializedProperty serializedProperty)
	{
		if (serializedProperty.type != typeof(RangeInt).Name)
		{
			Debug.LogError("SerializedProperty isn't of type " + typeof(RangeInt).Name);
		}

		int aValue = serializedProperty.FindPropertyRelative("_a").intValue;
		int bValue = serializedProperty.FindPropertyRelative("_b").intValue;
		return new RangeInt(aValue, bValue);
	}
#endif
}

[Serializable]
public struct RangeFloat
{
	[SerializeField] private float _a;

	[SerializeField] private float _b;

	public RangeFloat(
		float a,
		float b)
	{
		_a = a;
		_b = b;
	}

	public float Min => _a < _b ? _a : _b;
	public float Max => _b > _a ? _b : _a;

	public float RandomValue => Random.Range(Min, Max);

#if UNITY_EDITOR
	public static RangeFloat EditorGetValue(SerializedProperty serializedProperty)
	{
		if (serializedProperty.type != typeof(RangeFloat).Name)
		{
			Debug.LogError("SerializedProperty isn't of type " + typeof(RangeFloat).Name);
		}

		float aValue = serializedProperty.FindPropertyRelative("_a").floatValue;
		float bValue = serializedProperty.FindPropertyRelative("_b").floatValue;
		return new RangeFloat(aValue, bValue);
	}
#endif
}

#if UNITY_EDITOR
[CustomPropertyDrawer(typeof(RangeInt))]
[CustomPropertyDrawer(typeof(RangeFloat))]
public class RangeDrawer : PropertyDrawer
{
	public override void OnGUI(
		Rect position,
		SerializedProperty property,
		GUIContent label)
	{
		EditorGUI.BeginProperty(position, label, property);

		position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

		float labelWidth = EditorGUIUtility.labelWidth;
		int indentLevel = EditorGUI.indentLevel;

		EditorGUIUtility.labelWidth = 15f;
		EditorGUI.indentLevel = 0;

		float num = (position.width - 2f) / 2f;

		Rect position1 = new Rect(position)
		{
			width = num
		};
		Rect position2 = new Rect(position1)
		{
			x = position1.x + num + 2f
		};

		EditorGUI.PropertyField(position1, property.FindPropertyRelative("_a"), new GUIContent("A"));
		EditorGUI.PropertyField(position2, property.FindPropertyRelative("_b"), new GUIContent("B"));

		EditorGUIUtility.labelWidth = labelWidth;
		EditorGUI.indentLevel = indentLevel;

		EditorGUI.EndProperty();
	}
}
#endif // if UNITY_EDITOR