﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public static class FileExt
{
	public static void Save(
		string fileDirectory,
		string fileName,
		object obj)
	{
		if (!Directory.Exists(fileDirectory))
		{
			Directory.CreateDirectory(fileDirectory);
		}

		using (FileStream file = File.Create(fileDirectory + fileName))
        {
            BinaryFormatter bf = new BinaryFormatter();
            bf.Serialize(file, obj);
        }
	}

	public static T Load<T>(
		string fileDirectory,
		string fileName)
	{
		if (File.Exists(fileDirectory + fileName))
		{
			using (FileStream file = File.Open(fileDirectory + fileName, FileMode.Open))
	        {
	            BinaryFormatter bf = new BinaryFormatter();
	            T obj = (T) bf.Deserialize(file);
	            return obj;
	        }
		}
		else
		{
			return default(T);
		}
	}

	public static bool DeleteFile(string p_path)
	{
		bool sucess = false;

		try
		{
			File.Delete(p_path);
			sucess = true;
		}
		catch (Exception ex)
		{
			Debug.LogError("DeleteFile Error ! " + ex);
		}

		return sucess;
	}
}