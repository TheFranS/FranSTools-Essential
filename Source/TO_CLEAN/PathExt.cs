﻿using System.Collections.Generic;
using UnityEngine;
using System;

public static class PathExt
{
	public static Vector2[] ExtrudePath(
		Vector2[] path,
		float extrudeValue,
		int iteration = 0,
		bool flip = false)
	{
		if (path == null || path.Length < 2)
		{
			return path;
		}

		List<Vector2> extrudedPath = new List<Vector2>();

		for (int i = 0; i < path.Length; i++)
		{
			if (i == 0)
			{
				extrudedPath.Add(
					path[i] + Vector2.Perpendicular(path[i + 1] - path[i]).normalized *
					(!flip ? 1f : -1f) * extrudeValue);
			}
			else if (i == path.Length - 1)
			{
				extrudedPath.Add(
					path[i] + Vector2.Perpendicular(path[i] - path[i - 1]).normalized *
					(!flip ? 1f : -1f) * extrudeValue);
			}
			else
			{
				bool found;

				Vector2 firstLineA = path[i - 1];
				Vector2 firstLineB = path[i];
				Vector2 firstLineDirection = (firstLineB - firstLineA).normalized;
				Vector2 firstLinePerpendicular = Vector2.Perpendicular(firstLineDirection) * (!flip ? 1f : -1f);

				Vector2 firstLineAExtruded = firstLineA + firstLinePerpendicular * extrudeValue;
				Vector2 firstLineBExtruded = firstLineB + firstLinePerpendicular * extrudeValue;

				Vector2 secondLineA = path[i];
				Vector2 secondLineB = path[i + 1];
				Vector2 secondLineDirection = (secondLineB - secondLineA).normalized;
				Vector2 secondLinePerpendicular = Vector2.Perpendicular(secondLineDirection) * (!flip ? 1f : -1f);

				Vector2 secondLineAExtruded = secondLineA + secondLinePerpendicular * extrudeValue;
				Vector2 secondLineBExtruded = secondLineB + secondLinePerpendicular * extrudeValue;

				Vector2 intersection = Vector2Ext.GetIntersectionPointCoordinates(
					firstLineAExtruded,
					firstLineBExtruded,
					secondLineAExtruded,
					secondLineBExtruded,
					out found);

				Vector2 cornerNormal = firstLineDirection - secondLineDirection;
				Vector2 intersectionDirection = intersection - path[i];

				if (Vector2.Dot(cornerNormal, intersectionDirection) < 0f)
				{
					extrudedPath.Add(intersection);
				}
				else
				{
					extrudedPath.Add(firstLineBExtruded);
					for (int j = 0; j < iteration; j++)
					{
						Vector2 interpolatedDirection = Vector2.Lerp(
							firstLinePerpendicular,
							secondLinePerpendicular,
							(1f / (iteration + 1f)) * (j + 1)).normalized;

						extrudedPath.Add(path[i] + interpolatedDirection * extrudeValue);
					}

					extrudedPath.Add(secondLineAExtruded);
				}
			}
		}

		return extrudedPath.ToArray();
	}

	public static float CalculateLength(Vector2[] path)
	{
		if (path == null || path.Length < 2)
		{
			return 0f;
		}

		float length = 0f;

		for (int i = 1; i < path.Length; i++)
		{
			length += Vector2.Distance(path[i - 1], path[i]);
		}

		return length;
	}

	public static Vector2[] SmoothPath(Vector2[] path, float radius, int iteration = 0)
    {
        if (path == null)
        {
            throw new NullReferenceException();
        }

        if (path.Length <= 2)
        {
            return path;
        }
        
        if (radius <= 0f)
        {
            Debug.LogWarning("Radius must be > 0");
            return path;
        }

        if (iteration < 0)
        {
            Debug.LogWarning("Iteration must be >= 0");
            return path;
        }
        
        List<Vector2> newPath = new List<Vector2>();

        for (int i = 0; i < path.Length; i++)
        {
            if (i == 0 || i == path.Length - 1)
            {
                newPath.Add(path[i]);
            }
            else
            {
                Vector2 pointA = path[i - 1];
                Vector2 pointB = path[i];
                Vector2 pointC = path[i + 1];

                if (pointA == pointB || pointB == pointC)
                {
                    continue;
                }
                
                Vector2 abPerpendicular = Vector2.Perpendicular(pointB - pointA).normalized * radius;
                Vector2 bcPerpendicular = Vector2.Perpendicular(pointC - pointB).normalized * radius;

                if (Vector2Ext.Clockwise(abPerpendicular, bcPerpendicular))
                {
                    Vector2 center = Vector2Ext.GetIntersectionPointCoordinates(
                        pointA - abPerpendicular,
                        pointB - abPerpendicular,
                        pointB - bcPerpendicular,
                        pointC - bcPerpendicular,
                        out var centerFound);

                    Check.IsTrue(centerFound, "Internal error");

                    for (int j = 0; j < iteration + 2; j++)
                    {
                        Vector2 interpolatedDirection = Vector2.Lerp(
                            abPerpendicular,
                            bcPerpendicular,
                            (1f / (iteration + 1)) * j).normalized;

                        newPath.Add(center + interpolatedDirection * radius);
                    }
                }
                else
                {
                    newPath.Add(path[i]);
                }
            }
        }

        return newPath.ToArray();
    }
}