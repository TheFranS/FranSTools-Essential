﻿using UnityEngine;

public static class HideFlagsExt
{
	public static HideFlags HideAndDontSaveUnload
	{
		get
		{
			return HideFlags.DontSaveInEditor | HideFlags.DontSaveInBuild | HideFlags.NotEditable |
			       HideFlags.HideInHierarchy |
			       HideFlags.HideInInspector;
		}
	}
}