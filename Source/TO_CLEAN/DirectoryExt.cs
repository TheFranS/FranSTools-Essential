﻿using System.IO;

namespace FranS.IO
{
	public static class DirectoryExt
	{
		public static void Clean(string path)
		{
			DirectoryInfo directoryInfo = new DirectoryInfo(path);

			foreach (FileInfo file in directoryInfo.GetFiles())
			{
				file.Delete();
			}

			foreach (DirectoryInfo directory in directoryInfo.GetDirectories())
			{
				directory.Delete(true);
			}
		}
	}
} // namespace FranS.IO