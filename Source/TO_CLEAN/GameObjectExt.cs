﻿using UnityEngine;

public static class GameObjectExt
{
	public static GameObject[] GetChildrensRecursive(this GameObject gameObject)
	{
		Transform[] childrensTransform = gameObject.transform.GetComponentsInChildren<Transform>();
		GameObject[] childrensGameObject = new GameObject[childrensTransform.Length - 1]; // On enlève le parent

		// On ne récupère pas le parent (qui est premier du tableau), que les enfants !
		for (int i = 0; i < childrensGameObject.Length; i++)
		{
			childrensGameObject[i] = childrensTransform[i + 1].gameObject;
		}

		return childrensGameObject;
	}

	public static void DestroyImmediateWithChildrens(this GameObject gameObject)
	{
		GameObject[] childrens = gameObject.GetChildrensRecursive();
		for (int i = 0; i < childrens.Length; i++)
		{
			GameObject.DestroyImmediate(childrens[i]);
		}

		GameObject.DestroyImmediate(gameObject);
	}

	public static void SetHideFlagsWithChildrens(
		this GameObject gameObject,
		HideFlags passHideFlags)
	{
		GameObject[] childrens = gameObject.GetChildrensRecursive();
		for (int i = 0; i < childrens.Length; i++)
		{
			childrens[i].hideFlags = passHideFlags;
		}

		gameObject.hideFlags = passHideFlags;
	}

	public static Bounds GetBounds(GameObject gameObject)
	{
		Bounds newBounds = new Bounds();

		Renderer renderer = gameObject.GetComponent<Renderer>();
		if (renderer != null && renderer.bounds.size != Vector3.zero)
		{
			newBounds = renderer.bounds;
		}

		Collider collider = gameObject.GetComponent<Collider>();
		if (collider != null && collider.bounds.size != Vector3.zero)
		{
			if (renderer != null)
			{
				newBounds.Encapsulate(collider.bounds);
			}
			else
			{
				newBounds = collider.bounds;
			}
		}

		return newBounds;
	}

	public static Bounds GetBoundsWithChildrens(GameObject gameObject)
	{
		Bounds newBounds = GetBounds(gameObject);

		GameObject[] childrens = gameObject.GetChildrensRecursive();
		for (int i = 0; i < childrens.Length; i++)
		{
			Bounds getBounds = GetBounds(childrens[i]);
			if (getBounds.size != Vector3.zero)
			{
				if (newBounds.size != Vector3.zero)
				{
					newBounds.Encapsulate(getBounds);
				}
				else
				{
					newBounds = getBounds;
				}
			}
		}

		return newBounds;
	}

	public static GameObject[] GetAllGameObjectsInScene()
	{
		return Object.FindObjectsOfType<GameObject>();
	}
}