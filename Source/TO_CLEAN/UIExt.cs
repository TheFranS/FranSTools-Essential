﻿/*using DG.Tweening;
using System;
using UnityEngine;
using UnityEngine.UI;

public static class UIExt
{
	public static Tween ShowHideFadeElement(
		bool passTargetValue,
		float passTime,
		CanvasGroup passCanvasGroup,
		GameObject passGameObject
		)
	{
		// Fonction générique qui permet de cacher un élément de l'UI avec un fade et de désactiver son interactivitée durant la transition
		if (!passTargetValue)
		{
			passCanvasGroup.blocksRaycasts = false;
			return passCanvasGroup.DOFade(0, passTime).OnComplete(
				() => {
				passGameObject.SetActive(false);
			});
		}
		else
		{
			passGameObject.SetActive(true);
			return passCanvasGroup.DOFade(1, passTime).OnComplete(
				() => {
				passCanvasGroup.blocksRaycasts = true;
			});
		}
	}
}
*/

